import { Directive, Input, HostBinding, OnChanges } from "@angular/core";
import { States } from "./../enums/states.enum";
import { LowerCasePipe } from '@angular/common';

@Directive({
  selector: "[appState]"
})
export class StateDirective implements OnChanges {

  @Input() appState: string;
  @HostBinding("class") nomClass: string;

  constructor() {}

  ngOnChanges(){
    this.nomClass = this.formatClass(this.appState);
  }

  private formatClass(state: string): string {
    return `state-${state.normalize('NFD').replace(/[\u0300-\u036f\s]/g, '').toLowerCase()}`;
  }
}

// get un state
// en fonction du state, generate a class name
// bind this class name avec l'attribut class de l'element host
// (element sur lequel on applique cette directive)

// fn formatClass()
// Option => state-option
// Annulé => state-annule
// Confirmé => state-confirme
// ? Actif => state-actif
