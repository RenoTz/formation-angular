import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { TemplatesModule } from "../template/templates.module";
import { BoutonComponent } from "./components/bouton/bouton.component";
import { FormCommentComponent } from "./components/form-comment/form-comment.component";
import { NavPageComponent } from "./components/nav-page/nav-page.component";
import { TableauComponent } from "./components/tableau/tableau.component";
import { StateDirective } from "./directives/state.directive";
import { TotalPipe } from "./pipes/total.pipe";

@NgModule({
  declarations: [
    TotalPipe,
    TableauComponent,
    StateDirective,
    BoutonComponent,
    NavPageComponent,
    FormCommentComponent
  ],
  exports: [
    TotalPipe,
    TableauComponent,
    StateDirective,
    TemplatesModule,
    BoutonComponent,
    FormCommentComponent,
    NavPageComponent,
    FormsModule,
    ReactiveFormsModule
  ],
  imports: [
    CommonModule,
    TemplatesModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SharedModule {}
