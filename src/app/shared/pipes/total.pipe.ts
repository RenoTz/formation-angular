import { Prestation } from './../models/prestation.model';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'total'
})
export class TotalPipe implements PipeTransform {

  transform(value: Prestation, args?: any): number {
    return value? (args? value.totalTTC() : value.totalHT()) : null;
  }

}
