import { Component, Input, OnInit } from '@angular/core';
import { NavPage } from '../../models/nav-page.model';

@Component({
  selector: 'app-nav-page',
  templateUrl: './nav-page.component.html',
  styleUrls: ['./nav-page.component.scss']
})
export class NavPageComponent implements OnInit {

  @Input() listRoutes: NavPage[];

  constructor() { }

  ngOnInit() {
  }

}
