import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-form-comment',
  templateUrl: './form-comment.component.html',
  styleUrls: ['./form-comment.component.scss']
})
export class FormCommentComponent implements OnInit {

  @Input() comment: string;

  @Output() newCommment: EventEmitter<string> = new EventEmitter();

  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
    this.form.valueChanges.pipe(
      debounceTime(500)
    ).subscribe((data) => {
      this.newCommment.emit(data.comment);
    })
  }

  private createForm() {
    this.form = this.fb.group({
      comment: [this.comment, Validators.compose([Validators.required, Validators.maxLength(150)])]
    });
  }

}
