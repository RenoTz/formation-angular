import { States } from '../enums/states.enum';
import { PrestationsI } from './../interfaces/prestations-i';

export class Prestation implements PrestationsI {
  id: string;
  typePresta: string;
  client: string;
  tjmHt = 500;
  nbJours = 0;
  tauxTva = 20;
  comment: string;
  state =  States.OPTION;
  totalHT(): number {
    return this.tjmHt * this.nbJours;
  }
  totalTTC(tva?: number): number {
    if(tva <= 0) {
      return this.totalHT();
    }
    return tva? this.totalHT() * (1 + tva / 100) : this.totalHT() * (1 + this.tauxTva / 100) ;
  }

  constructor(field?: Partial<Prestation>) {
    if(field) {
      Object.assign(this, field);
    }
  }

}
