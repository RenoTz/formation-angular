import { NavPageI } from '../interfaces/nav-page-i';

export class NavPage implements NavPageI {
  route: string;
  label: string;
  constructor(field?: Partial<NavPage>) {
    if(field) {
      Object.assign(this, field);
    }
  }
}
