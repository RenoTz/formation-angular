import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { States } from './../../../shared/enums/states.enum';
import { Prestation } from './../../../shared/models/prestation.model';
import { PrestationService } from './../../services/prestation.service';

@Component({
  selector: 'app-item-prestation',
  templateUrl: './item-prestation.component.html',
  styleUrls: ['./item-prestation.component.scss']
})
export class ItemPrestationComponent implements OnInit {

  @Input() item: Prestation;

  @Output() itemAndState: EventEmitter<Prestation> = new EventEmitter();
  @Output() clicked: EventEmitter<{}> = new EventEmitter();

  public states = States;

  colorTd = false;

  constructor(
    private ps: PrestationService,
    private router: Router
  ) {
  }

  ngOnInit() {  }

  update() {
    this.itemAndState.emit(this.item);
  }

  getDetail() {
    this.clicked.emit();
    this.ps.presta$.next(this.item);
    this.colorTd = true;
  }

  removeClass() {
    this.colorTd = false;
  }

  redirectEditItem() {
    this.router.navigate(['prestations/edit', this.item.id]);
  }

}
