import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { States } from './../../../shared/enums/states.enum';
import { Prestation } from './../../../shared/models/prestation.model';

@Component({
  selector: 'app-form-prestation',
  templateUrl: './form-prestation.component.html',
  styleUrls: ['./form-prestation.component.scss']
})
export class FormPrestationComponent implements OnInit {

  states = States;

  @Output() nItem: EventEmitter<Prestation> = new EventEmitter();

  @Input() presta: Prestation;

  init = new Prestation();
  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    if(this.presta){
      this.init = this.presta;
    }
    this.createForm()
  }

  createForm() {
    this.form = this.fb.group({
      typePresta: [this.init.typePresta, Validators.required],
      client: [this.init.client, Validators.compose([Validators.required, Validators.minLength(3)])],
      tjmHt: [this.init.tjmHt],
      nbJours: [this.init.nbJours],
      tauxTva: [this.init.tauxTva],
      comment: [this.init.comment],
      state: [this.init.state]
    });
  }

  onSubmit() {
    this.nItem.emit(this.form.value);
  }

}
