import { NavPage } from './../../../shared/models/nav-page.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-prestations',
  templateUrl: './page-prestations.component.html',
  styleUrls: ['./page-prestations.component.scss']
})
export class PagePrestationsComponent implements OnInit {

  title = 'Prestations';
  navPage = [
    new NavPage({ route: 'detail', label: 'Detail'}),
    new NavPage({ route: 'comment', label: 'Commentaires'})
  ]

  constructor() { }

  ngOnInit() {
  }

}
