import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from "rxjs";
import { Prestation } from "src/app/shared/models/prestation.model";
import { PrestationService } from "../../services/prestation.service";

@Component({
  selector: "app-comment-prestation",
  templateUrl: "./comment-prestation.component.html",
  styleUrls: ["./comment-prestation.component.scss"]
})
export class CommentPrestationComponent implements OnInit {

  constructor(
    public ps: PrestationService
  ) {
  }

  ngOnInit() {
  }

  addComment(comment: string) {
    const presta = this.ps.presta$.value;
    presta.comment = comment;
    this.ps.update(presta);
  }

}
