import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Prestation } from 'src/app/shared/models/prestation.model';
import { PrestationService } from '../../services/prestation.service';

@Component({
  selector: 'app-edit-prestation',
  templateUrl: './edit-prestation.component.html',
  styleUrls: ['./edit-prestation.component.scss']
})
export class EditPrestationComponent implements OnInit {

  public presta$: Observable<Prestation>;

  idPesta: string;

  constructor(
    private ps: PrestationService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.idPesta = params.id;
      this.presta$ = this.ps.getPrestation(params.id);
    });
  }

  edit(item: Prestation) {
    item.id = this.idPesta;
    this.ps.update(item).then((data) => {
      this.router.navigate(['/prestations']);
    });
  }

}
