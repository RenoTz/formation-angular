import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemPrestationComponent } from '../../components/item-prestation/item-prestation.component';
import { Prestation } from './../../../shared/models/prestation.model';
import { PrestationService } from './../../services/prestation.service';

@Component({
  selector: 'app-list-prestations',
  templateUrl: './list-prestations.component.html',
  styleUrls: ['./list-prestations.component.scss']
})
export class ListPrestationsComponent implements OnInit, OnDestroy {

  //public collection: Prestation[];
  public collection$: Observable<Prestation[]>;

  @ViewChildren(ItemPrestationComponent) items: QueryList<ItemPrestationComponent>;

  labelButton = 'Ajouter une prestation';

  public headers = [
    'type',
    'client',
    'nb jours',
    'tjm ht',
    'total ht',
    'total ttc',
    'state'
  ]

  //sub: Subscription;

  constructor(private ps: PrestationService) {
  }

  ngOnInit() {
    this.collection$ = this.ps.collection$;
    //this.collection = this.ps.collection;
    // this.sub = this.ps.collection$.subscribe((data) => {
    //     this.collection = data;
    // });
  }

  update(item: Prestation){
    this.ps.update(item).then((data) => {
      // traiter retour api
    });
  }

  removeClassActive() {
    this.items.forEach(item => {
      item.removeClass();
    });
  }

  ngOnDestroy() {
    //this.sub.unsubscribe();
  }
}
