import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { FormPrestationComponent } from "./components/form-prestation/form-prestation.component";
import { ItemPrestationComponent } from "./components/item-prestation/item-prestation.component";
import { AddPrestationComponent } from "./containers/add-prestation/add-prestation.component";
import { CommentPrestationComponent } from "./containers/comment-prestation/comment-prestation.component";
import { DetailPrestationComponent } from "./containers/detail-prestation/detail-prestation.component";
import { ListPrestationsComponent } from "./containers/list-prestations/list-prestations.component";
import { PageAddPrestationComponent } from "./pages/page-add-prestation/page-add-prestation.component";
import { PagePrestationsComponent } from "./pages/page-prestations/page-prestations.component";
import { PrestationsRoutingModule } from "./prestations-routing.module";
import { PageEditPrestationComponent } from './pages/page-edit-prestation/page-edit-prestation.component';
import { EditPrestationComponent } from './containers/edit-prestation/edit-prestation.component';

@NgModule({
  declarations: [
    PagePrestationsComponent,
    ListPrestationsComponent,
    ItemPrestationComponent,
    DetailPrestationComponent,
    CommentPrestationComponent,
    PageAddPrestationComponent,
    AddPrestationComponent,
    FormPrestationComponent,
    PageEditPrestationComponent,
    EditPrestationComponent
  ],
  imports: [CommonModule, PrestationsRoutingModule, SharedModule]
})
export class PrestationsModule {}
