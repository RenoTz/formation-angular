import { Prestation } from './../../shared/models/prestation.model';

export const fakePrestations: Prestation[] = [
  new Prestation({
    id: 'kjzdlk',
    typePresta: 'Formation',
    client: 'Capgemini',
    tjmHt: 3000,
    nbJours: 25,
    comment: 'Merci cap pour ces 25j à 3K'
  }),
  new Prestation({
    id: 'lkjmlk',
    typePresta: 'Development',
    client: 'Capgemaxi',
    tjmHt: 4000,
    nbJours: 35,
    comment: 'Merci cap pour ces 35j à 4K'
  })
];
