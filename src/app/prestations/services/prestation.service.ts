import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Prestation } from './../../shared/models/prestation.model';

@Injectable({
  providedIn: 'root'
})
export class PrestationService {

  private _collection$: Observable<Prestation[]>;
  private itemsCollection: AngularFirestoreCollection<Prestation>;
  presta$: BehaviorSubject<Prestation> = new BehaviorSubject(null);

  constructor(
    private afs: AngularFirestore,
    private http: HttpClient
  ) {
    //this.collection = fakePrestations;
    this.itemsCollection = afs.collection<Prestation>('prestations');
    this._collection$ = this.itemsCollection.valueChanges().pipe(
      //map(data => data.map(obj => new Prestation(obj)))
      map((data) => {
        this.presta$.next(new Prestation(data[0]));
        return data.map((obj) => {
          return new Prestation(obj);
        })
      })
    );

    // exemple d'appel API REST
    /* this.collection$ = this.http.get<Prestation[]>(`${URL_API}/prestations`).pipe(
      map(data => data.map(obj => new Prestation(obj)))
    ); */
  }

  public get collection$(): Observable<Prestation[]> {
    return this._collection$;
  }

  public set collection$(col$: Observable<Prestation[]>){
    this._collection$ = col$;
  }

  // add presta
  add(item: Prestation): Promise<any> {
    const id = this.afs.createId();
    const prestation = { id, ...item };
    return this.itemsCollection.doc(id).set(prestation).catch((e) => {
      console.log(e);
    });
    // faire un suscribe pour déclencher l'appel REST depuis le component
    // return this.http.post(`${URL_API}/prestations`, item);
  }


  update(item: Prestation): Promise<any> {
    const presta = { ...item};
    return this.itemsCollection.doc(item.id).update(presta).catch((e) => {
      console.log(e);
    });
    // faire un suscribe pour déclencher l'appel REST depuis le component
    // return this.http.patch(`${URL_API}/prestations/${item.id}`, item);
  }

  public delete(item: Prestation): Promise<any> {
    return this.itemsCollection.doc(item.id).delete().catch((e) => {
      console.log(e);
    });
    // faire un suscribe pour déclencher l'appel REST depuis le component
    // return this.http.delete(`${URL_API}/prestations/${item.id}`);
  }

  getPrestation(id: string): Observable<Prestation> {
    return this.itemsCollection.doc<Prestation>(id).valueChanges();
    // faire un suscribe pour déclencher l'appel REST depuis le component
    // return this.http.get(`${URL_API}/prestations/${id}`);
  }
}
