import { StateClient } from './../../shared/enums/state-client.enum';
import { Client } from 'src/app/shared/models/client.model';

export const fakeClients: Client[] = [
  new Client({
    id: 'kzjlkjez',
    name:  'Jean-Michel',
    email: 'jean-michel@mail.com'
  }),
  new Client({
    id: 'kzjlkjez',
    name:  'Jean-Jacques',
    email: 'jean-jacques@mail.com',
    state: StateClient.INACTIF
  })
];
