import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { SharedModule } from "../shared/shared.module";
import { ClientsRoutingModule } from "./clients-routing.module";
import { ItemClientComponent } from "./components/item-client/item-client.component";
import { ListClientsComponent } from "./containers/list-clients/list-clients.component";
import { PageClientsComponent } from "./pages/page-clients/page-clients.component";
import { PageAddClientComponent } from "./pages/page-add-client/page-add-client.component";
import { AddClientComponent } from "./containers/add-client/add-client.component";
import { FormClientComponent } from "./components/form-client/form-client.component";

@NgModule({
  declarations: [
    ListClientsComponent,
    PageClientsComponent,
    ItemClientComponent,
    PageAddClientComponent,
    AddClientComponent,
    FormClientComponent
  ],
  imports: [CommonModule, ClientsRoutingModule, SharedModule]
})
export class ClientsModule {}
