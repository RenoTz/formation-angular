import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from 'src/app/shared/models/client.model';
import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-list-clients',
  templateUrl: './list-clients.component.html',
  styleUrls: ['./list-clients.component.scss']
})
export class ListClientsComponent implements OnInit {

  //public collection: Client[];
  public collection$: Observable<Client[]>;

  labelButton = 'Ajouter un client';

  public headers = [
    'name',
    'email',
    'state'
  ]

  constructor(private cs: ClientService) {
  }

  ngOnInit() {
    this.collection$ = this.cs.collection$;
  }

  update(item: Client){
    this.cs.update(item).then((data) => {
      // traiter retour api
    });
  }
}
