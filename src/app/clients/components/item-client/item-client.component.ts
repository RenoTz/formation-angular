import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Client } from 'src/app/shared/models/client.model';
import { StateClient } from './../../../shared/enums/state-client.enum';

@Component({
  selector: 'app-item-client',
  templateUrl: './item-client.component.html',
  styleUrls: ['./item-client.component.scss']
})
export class ItemClientComponent implements OnInit {

  @Input() item: Client;

  @Output() itemAndState: EventEmitter<Client> = new EventEmitter();

  public states = StateClient;

  constructor() {
  }

  ngOnInit() {  }

  update() {
    this.itemAndState.emit(this.item);
  }

}
