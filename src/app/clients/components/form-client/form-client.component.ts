import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Client } from 'src/app/shared/models/client.model';
import { StateClient } from './../../../shared/enums/state-client.enum';

@Component({
  selector: 'app-form-client',
  templateUrl: './form-client.component.html',
  styleUrls: ['./form-client.component.scss']
})
export class FormClientComponent implements OnInit {

  states = StateClient;

  @Output() nItem: EventEmitter<Client> = new EventEmitter();

  init = new Client();
  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.createForm()
  }

  createForm() {
    this.form = this.fb.group({
      name: [this.init.name],
      email: [this.init.email],
      state: [this.init.state]
    });
  }

  onSubmit() {
    this.nItem.emit(this.form.value);
  }

}
