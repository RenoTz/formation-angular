import { TestBed } from '@angular/core/testing';

import { CapUiService } from './cap-ui.service';

describe('CapUiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CapUiService = TestBed.get(CapUiService);
    expect(service).toBeTruthy();
  });
});
