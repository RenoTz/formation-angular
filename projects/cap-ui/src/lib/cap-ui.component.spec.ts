import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapUiComponent } from './cap-ui.component';

describe('CapUiComponent', () => {
  let component: CapUiComponent;
  let fixture: ComponentFixture<CapUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
