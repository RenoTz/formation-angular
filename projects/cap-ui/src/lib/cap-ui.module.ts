import { NgModule } from '@angular/core';
import { CapUiComponent } from './cap-ui.component';

@NgModule({
  declarations: [CapUiComponent],
  imports: [
  ],
  exports: [CapUiComponent]
})
export class CapUiModule { }
